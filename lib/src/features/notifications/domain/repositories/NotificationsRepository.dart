import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/util/error/failures.dart';
import 'package:progiom_cms/src/features/notifications/domain/entities/ServerNotification.dart';

abstract class NotificationsRepository {
  Future<Either<Failure, List<ServerNotification>>> getMyNotifications(
      int page);
  Future<Either<Failure, bool>> setNotifiationAsRead(String notificationId);
  Future<Either<Failure, bool>> disableEnableNotification(
      {required bool enable});
  Future<Either<Failure, bool>> disableEnableOffersNotification(
      {required bool enable});
  Future<Either<Failure, bool>> disableEnableDiscountNotification(
      {required bool enable});
}
