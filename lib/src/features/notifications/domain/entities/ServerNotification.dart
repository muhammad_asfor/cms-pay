import 'package:json_annotation/json_annotation.dart';

part 'ServerNotification.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ServerNotification {
  final String id;
  final String createdAt;
  final String notificationTitle;
  final String notificationText;
  final String notificationType;
  final bool isRead;
  final bool isSeen;
  final String? entityType;
  final int? entityId;
  final String? link;
  final String? image;

  ServerNotification(
      {required this.id,
      required this.createdAt,
      required this.notificationTitle,
      required this.notificationText,
      required this.notificationType,
      required this.isRead,
      required this.isSeen,
      this.entityType,
      this.entityId,
      this.link,
      this.image});

  factory ServerNotification.fromJson(json) =>
      _$ServerNotificationFromJson(json);
  toJson() => _$ServerNotificationToJson(this);
  static List<ServerNotification> fromJsonList(List json) {
    return json.map((e) => ServerNotification.fromJson(e)).toList();
  }
}
