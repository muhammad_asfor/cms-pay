import 'dart:convert';

import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeSettingsLocalDataSource {
  final SharedPreferences _preferences;

  HomeSettingsLocalDataSource(this._preferences);

  Future<Map?> getPreferences() async {
    final preferences =
        _preferences.getString(HomeSettingsConstants.preferences);
    return preferences == null || preferences == ''
        ? null
        : json.decode(preferences);
  }

  setPreferences(Map preferences) {
    _preferences.setString(
        HomeSettingsConstants.preferences, json.encode(preferences));
  }
}

class HomeSettingsConstants {
  static const String preferences = "preferences";
}
