import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:launch_review/launch_review.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/src/features/core/util/ios_version.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../homeSettings.dart';
import '../bloc/homesettings_bloc.dart';
// import 'dart:developer' as dev;

class HomeSettingsWrapper extends StatefulWidget {
  final _HomeSettingsWrapperState _state = _HomeSettingsWrapperState();

  getSettings({int? countryId}) => _state.getSttings(countryId: countryId);

  HomeSettingsWrapper({
    Key? key,
    this.child,
    required this.failureCallback,
    required this.successCallback,
  }) : super(key: key);
  final Function() successCallback;
  final Function(String error) failureCallback;
  Widget? child;

  @override
  _HomeSettingsWrapperState createState() => _state;
}

class _HomeSettingsWrapperState extends State<HomeSettingsWrapper> {
  getSttings({int? countryId}) {
    sl<HomesettingsBloc>().add(GetSettings(countryId: countryId));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: sl<HomesettingsBloc>(),
      listener: (context, state) {
        if (state is HomeSettingsReady) {
          String appVersion = Platform.isIOS
              ? getIosVersionNumber(state.packageInfo)
              : state.packageInfo.version;

          String appLatestVersion =
              sl<HomesettingsBloc>().settings?.appLatestVersion?.toString() ??
                  "";
          String appMinVersion =
              sl<HomesettingsBloc>().settings?.appMinVersion?.toString() ?? "";
          if ((double.tryParse(appVersion) ?? 1.0) >=
              double.parse(appLatestVersion)) {
            widget.successCallback();
          } else if ((double.tryParse(appVersion) ?? 1.0) >=
              double.parse(appMinVersion)) {
            showUpdateAvailable(
              state,
            );
          } else {
            showNeedUpdate();
          }
        }
        if (state is ErrorInGettingSettings) {
          widget.failureCallback(state.error);
        }
      },
      child: widget.child,
    );
  }

  Future showUpdateAvailable(
    state,
  ) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("تحديث جديد"),
            content: Text("يتوفر تحديث جديد على المتجر , هل توَد التحديث ؟"),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    widget.successCallback();
                  },
                  child: Text("ليس الاَن")),
              TextButton(
                  onPressed: () {
                    openStore();
                  },
                  child: Text("تحديث"))
            ],
          );
        });
  }

  Future showNeedUpdate() {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("تحديث التطبيق"),
            content: Text("نسخة التطبيق أصبحت قديمة, يجب التحديث للمتابعة."),
            actions: [
              TextButton(
                  onPressed: () {
                    openStore();
                  },
                  child: Text("تحديث"))
            ],
          );
        });
  }

  void openStore() {
    LaunchReview.launch(writeReview: false);
  }
}
