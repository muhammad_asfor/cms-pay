import 'package:dartz/dartz.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class GetPages extends UseCase<List<PageModel>, NoParams> {
  final SettingsRepository repository;

  GetPages(
    this.repository,
  );

  @override
  Future<Either<Failure, List<PageModel>>> call(NoParams params) async {
    return await repository.getPages();
  }
}
