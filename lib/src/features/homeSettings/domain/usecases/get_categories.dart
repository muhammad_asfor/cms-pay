import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';

import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class GetCategories
    extends UseCase<List<Category>, NoParams> {
  final SettingsRepository repository;

  GetCategories(
    this.repository,
  );

  @override
  Future<Either<Failure, List<Category>>> call(
      NoParams params) async {
    return await repository.getCategories();
  }
}