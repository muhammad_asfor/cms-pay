import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';

import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class GetSliders extends UseCase<List<Slide>, NoParams> {
  final SettingsRepository repository;

  GetSliders(
    this.repository,
  );

  @override
  Future<Either<Failure, List<Slide>>> call(NoParams params) async {
    return await repository.getMobileSlides();
  }
}
