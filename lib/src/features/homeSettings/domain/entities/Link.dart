import 'package:json_annotation/json_annotation.dart';
part 'Link.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Link {
  final String? url;
  final String? label;
  final bool? active;

  Link({
    this.url,
    this.label,
    this.active,
  });

  factory Link.fromJson(json) => _$LinkFromJson(json);
  toJson() => _$LinkToJson(this);
}
