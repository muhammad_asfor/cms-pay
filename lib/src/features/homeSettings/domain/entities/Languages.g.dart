// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Languages.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Languages _$LanguagesFromJson(Map<String, dynamic> json) => Languages(
      id: json['id'] as int?,
      flag: (json['flag'] as List<dynamic>?)?.map((e) => e as String).toList(),
      slug: json['slug'] as String?,
      isActive: json['is_active'] as bool?,
      coverImage: json['cover_image'] as String?,
      title: json['title'] as String?,
    );

Map<String, dynamic> _$LanguagesToJson(Languages instance) => <String, dynamic>{
      'id': instance.id,
      'flag': instance.flag,
      'slug': instance.slug,
      'is_active': instance.isActive,
      'cover_image': instance.coverImage,
      'title': instance.title,
    };
