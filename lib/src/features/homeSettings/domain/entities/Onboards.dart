
import 'package:json_annotation/json_annotation.dart';
part 'Onboards.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Onboards {
  int id;
  final String? coverImage;
  final String? title;
  final String? description;
  factory Onboards.fromJson(json) => _$OnboardsFromJson(json);
  toJson() => _$OnboardsToJson(this);
  Onboards({required this.id, this.coverImage, this.title, this.description});
}
