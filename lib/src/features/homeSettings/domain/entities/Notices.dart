import 'package:json_annotation/json_annotation.dart';
part 'Notices.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Notices {
  final String? type;
  final String? title;
  final String? text;
  factory Notices.fromJson(json) => _$NoticesFromJson(json);
  toJson() => _$NoticesToJson(this);
  Notices({required this.type, required this.title, required this.text});
}
