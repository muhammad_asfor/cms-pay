import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/BaseListResponse.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Category.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Product.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/service.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/PageModel.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/blog.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/currency.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/filter_data.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/slide.dart';

import 'CategoryFeatured.dart';
import 'Languages.dart';
import 'Notices.dart';
import 'Onboards.dart';
import 'dart:developer' as dev;
part 'SettingsModel.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class SettingsModel {
  final List<Languages> languages;
  final List<Onboards> onboards;
  final List<Category>? categories;
  final List<CategoryFeatured>? categoriesFeatured;
  @JsonKey(fromJson: baseProductFromJson)
  final BaseListResponse<Product>? recentProducts;
  @JsonKey(fromJson: baseProductFromJson)
  final BaseListResponse<Product>? featuredProducts;
  final List<Product>? selectedProducts;
  User? user;

  int? unreadNotifications;
  final List<Notices>? notices;
  @JsonKey(fromJson: baseBlogFromJson)
  final BaseListResponse<Blog>? blogPosts;
  @JsonKey(fromJson: versionFromJson)
  final String? appLatestVersion;
  @JsonKey(fromJson: versionFromJson)
  final String? appMinVersion;
  final FilterData? filterData;
  List<PageModel>? pages;
  final List<Slide>? slides;

  final List<Currency>? currencies;

  // List<Strings> strings;

  /// For Batal
  final List<Service>? featuredServices;

  SettingsModel({
    this.selectedProducts,
    this.recentProducts,
    this.filterData,

    this.featuredProducts,
    this.pages,
    this.slides,
    this.blogPosts,
    required this.languages,
    required this.onboards,
    this.categories,
    this.categoriesFeatured,
    this.user,
    this.unreadNotifications,
    this.notices,
    this.appLatestVersion,
    this.appMinVersion,
    required this.currencies,
    this.featuredServices,
  });

  factory SettingsModel.fromJson(json) => _$SettingsModelFromJson(json);
  toJson() => _$SettingsModelToJson(this);
  static baseProductFromJson(json) {
    if (json != null)
      return BaseListResponse<Product>.fromJson(
          json, (data) => Product.fromJson(data));
  }

  static baseBlogFromJson(json) {
    if (json != null)
      return BaseListResponse<Blog>.fromJson(
          json, (data) => Blog.fromJson(data));
  }

  static versionFromJson(json) {
    if (json != null) return json.toString();
  }
}
