import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/ecommerce.dart';

class EcommerceApi {
  Future<Product> getProductDetails({required String id}) async {
    try {
      return Product.fromJson((await sl<Dio>().get(
        "api/posts/$id",
      ))
          .data);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<List<Product>> getListProducts(
      {required String endpoint, Map<int, dynamic>? filterValues}) async {
    try {
      String values = '';
      filterValues?.forEach((key, value) {
        if (value is List) {
          for (var e in value) {
            values += '&_field_values[$key][]=$e';
          }
        } else {
          values += '&_field_values[$key][]=$value';
        }
      });
      return Product.fromJsonList((await sl<Dio>()
              .get("api/" + endpoint + (filterValues != null ? values : '')))
          .data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<List<FavoriteItem>> getFavorites({required int page}) async {
    try {
      return FavoriteItem.fromJsonList(
          (await sl<Dio>().get("api/favorites?page=$page")).data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> setIsFavorites(
      {required bool isFavorite, required String itemId}) async {
    try {
      await sl<Dio>().post("api/favorites", data: {
        "item_type": "posts",
        "item_id": itemId,
        "action": isFavorite ? "1" : "0"
      });
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> cancelOrder(int orderId) async {
    try {
      await sl<Dio>().post("api/cancel_order", data: {
        "order_id": orderId,
      });
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<List<OrderItem>> getOrders({required int page}) async {
    try {
      return OrderItem.fromJsonList((await sl<Dio>().get(
        "api/orders?page=$page",
      ))
          .data['data']);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<String> url2id(String url) async {
    try {
      return ((await sl<Dio>().get(
        "api/url2id?q=$url",
      ))
          .data['data']
          .toString());
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<List<Address>> getListAddresses() async {
    try {
      return Address.fromJsonList(
          (await sl<Dio>().get("api/addresses")).data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<List<PointsRegistry>> getMyPointsRegistry() async {
    try {
      return PointsRegistry.fromJsonList(
          (await sl<Dio>().get("api/my-points/registry")).data["data"]
              ["items"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> addPost({
    // required int cityId,
    // required int countryId,
    // int? stateId,
    required int parentId,
    required String title,
    required String description,
    required List<File> images,
  }) async {
    try {
      List<MultipartFile> files = [];
      for (var e in images) {
        String fileName = e.path.split('/').last;
        files.add(await MultipartFile.fromFile(e.path, filename: fileName));
      }
      FormData formData = FormData.fromMap({
        "parent_id": parentId,
        "title": title,
        "description": description,
        "images": files,
      });
      await sl<Dio>().post("api/posts", data: formData);
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> addAddress({
    required int cityId,
    required int countryId,
    int? stateId,
    required String name,
    required String description,
  }) async {
    try {
      await sl<Dio>().post("api/addresses", data: {
        "cityId": cityId,
        "countryId": countryId,
        "name": name,
        "description": description,
        "stateId": stateId ?? ""
      });
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> updateAddress(
      {required int cityId,
      required int countryId,
      required String name,
      required String description,
      required int id}) async {
    try {
      await sl<Dio>().post("api/addresses/$id", data: {
        "cityId": cityId,
        "countryId": countryId,
        "name": name,
        "description": description,
        "_method": "PUT"
      });
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> deleteAddress(int id) async {
    try {
      await sl<Dio>().post("api/addresses/$id", data: {"_method": "DELETE"});
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> setDefaultAddress(int addressId) async {
    try {
      await sl<Dio>().post("api/addresses/$addressId",
          data: {"is_default": "1", "_method": "PUT"});
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<Address> getAddressDetails(int id) async {
    try {
      return Address.fromJson((await sl<Dio>().get(
        "api/addresses/$id",
      ))
          .data);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> reviewOrderProduct(
      {required int itemId,
      required int rating,
      required String comment}) async {
    try {
      await sl<Dio>().post("api/reviews", data: {
        "item_type": "OrderItem",
        "item_id": itemId,
        "rate": rating,
        "comment": comment
      });
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<OrderItem> getOrderDetails(int id) async {
    try {
      return OrderItem.fromJson((await sl<Dio>().get(
        "api/orders/$id",
      ))
          .data);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<Map<String, dynamic>> checkout(CheckoutModel checkoutModel) async {
    try {
      Response respn =
          await sl<Dio>().post("api/orders", data: checkoutModel.toJson());
      return respn.data;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<CartModel> getCart() async {
    try {
      return CartModel.fromJson((await (sl<Dio>().get(
        "api/carts",
      )))
          .data);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> addToCart(
      {required int productId,
      required int qty,
      Map<String, String>? selectedOptions}) async {
    try {
      final Map<String, dynamic> data = {
        "item_id": productId,
        "qty": qty,
      };
      if (selectedOptions != null && selectedOptions.keys.isNotEmpty) {
        data.putIfAbsent("options", () => selectedOptions);
      }

      await sl<Dio>().post("api/carts", data: data);
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<UseCouponResponse> useCoupon({
    required String code,
  }) async {
    try {
      return UseCouponResponse.fromJson(
          (await sl<Dio>().post("api/coupons/apply", data: {"code": code}))
              .data);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> checkoutPoints(
      int addressId, int itemId, Map? selectedOptions) async {
    try {
      final Map<String, dynamic> data = {
        "item_id": itemId,
        "address_id": addressId,
      };
      if (selectedOptions != null && selectedOptions.keys.isNotEmpty) {
        data.putIfAbsent("options", () => selectedOptions);
      }
      await sl<Dio>().post("api/my-points/buy", data: data);
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<MyPointsResponse> getMyPoints() async {
    try {
      return MyPointsResponse.fromJson((await sl<Dio>().get(
        "api/user/my-points",
      ))
          .data);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> updateCartItem({
    required int cartItemId,
    required int qty,
  }) async {
    try {
      await sl<Dio>()
          .post("api/carts/$cartItemId", data: {"qty": qty, "_method": "PUT"});
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> deleteFromCart({
    required int cartItemId,
  }) async {
    try {
      await sl<Dio>()
          .post("api/carts/$cartItemId", data: {"_method": "DELETE"});
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<List<CountryModel>> getCountries() async {
    try {
      return CountryModel.fromJsonList(
          (await sl<Dio>().get("/api/countries?page=1&length=1000"))
              .data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<List<StatesModel>> getStates(int countryId) async {
    try {
      return StatesModel.fromJsonList(
          (await sl<Dio>().get("/api/states?length=2500&country_id=$countryId"))
              .data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<List<CityModel>> getCities({int? countryId, int? stateId}) async {
    try {
      if (stateId != null) {
        return CityModel.fromJsonList(
            (await sl<Dio>().get("/api/cities?state_id=$stateId&length=2500"))
                .data["data"]);
      }
      return CityModel.fromJsonList(
          (await sl<Dio>().get("/api/cities?country_id=$countryId&length=2500"))
              .data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }
}
