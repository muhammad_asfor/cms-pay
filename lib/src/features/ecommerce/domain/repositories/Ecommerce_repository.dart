import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/entities/Address.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/CartModel.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/UseCouponResponse.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/cityModel.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/CountryModel.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/FavoriteItem.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/OrderItem.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/CheckoutModel.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/my_points_response.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/points_registry.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/states_model.dart';

abstract class EcommerceRepository {
  Future<Either<Failure, List<Product>>> getListProducts(
      {required String endpoint, Map<int, dynamic>? filterValues});
  Future<void> saveLocalQuery({required String query});
  List<String>? getLocalQueries();
  void deleteSearchHistoryItem(String query);
  Future<Either<Failure, List<FavoriteItem>>> getFavorites({required int page});
  Future<Either<Failure, bool>> setIsFavorites(
      {required bool isFavorite, required String itemId});
  Future<Either<Failure, bool>> cancelOrder({required int orderId});

  Future<Either<Failure, bool>> reviewOrderProduct(
      {required int itemId, required int rating, required String comment});
  Future<Either<Failure, List<OrderItem>>> getOrders({required int page});
  Future<Either<Failure, OrderItem>> getOrderDetails({required int orderId});

  Future<Either<Failure, bool>> addPost({
    required int parentId,
    required String title,
    required String description,
    required List<File> images,
  });
  Future<Either<Failure, bool>> addAddress({
    required int cityId,
    required int countryId,
    int? stateId,
    required String name,
    required String description,
  });
  Future<Either<Failure, bool>> updateAddress({
    required int cityId,
    required int id,
    required int countryId,
    required String name,
    required String description,
  });
  Future<Either<Failure, bool>> deleteAddress(int id);
  Future<Either<Failure, Address>> getAddressDetails(int id);
  Future<Either<Failure, List<Address>>> getMyAddresses();
  Future<Either<Failure, bool>> setDefaultAddress({required int addressId});
  Future<Either<Failure, Address>> getDefaultAddress();

  Future<Either<Failure, Product>> getProductDetails({required String id});
  Future<Either<Failure, bool>> checkoutPoints(
      int addressId, int itemId, Map? selectedOptions);

  Future<Either<Failure, Map<String, dynamic>>> checkout(
      CheckoutModel checkoutModel);
  Future<Either<Failure, CartModel>> getCart();
  Future<Either<Failure, bool>> addToCart(
      {required int productId,
      required int qty,
      Map<String, String>? selectedOptions});
  Future<Either<Failure, bool>> updateCartItem({
    required int cartItemId,
    required int qty,
  });
  Future<Either<Failure, bool>> deleteFromCart({
    required int cartItemId,
  });
  Future<Either<Failure, UseCouponResponse>> useCoupon({
    required String code,
  });

  Future<Either<Failure, List<CountryModel>>> getCountries();
  Future<Either<Failure, List<CityModel>>> getCities(
      {int? countryId, int? stateId});
  Future<Either<Failure, List<StatesModel>>> getStates(int countryId);

  Future<Either<Failure, String>> url2id(String url);
  Future<Either<Failure, MyPointsResponse>> getMyPoints();
  Future<Either<Failure, List<PointsRegistry>>> getMyPointsRegistry();
}
