import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class CancelOrder extends UseCase<bool, CancelOrderParams> {
  final EcommerceRepository repository;
  CancelOrder(this.repository);

  @override
  Future<Either<Failure, bool>> call(CancelOrderParams params) async {
    return await repository.cancelOrder(orderId: params.orderId);
  }
}

class CancelOrderParams {
  final int orderId;

  CancelOrderParams({
    required this.orderId,
  });
}
