import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/OrderItem.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetOrders extends UseCase<List<OrderItem>, GetOrdersParams> {
  final EcommerceRepository repository;
  GetOrders(this.repository);

  @override
  Future<Either<Failure, List<OrderItem>>> call(GetOrdersParams params) async {
    return await repository.getOrders(page: params.page);
  }
}

class GetOrdersParams {
  final int page;

  GetOrdersParams({
    required this.page,
  });
}
