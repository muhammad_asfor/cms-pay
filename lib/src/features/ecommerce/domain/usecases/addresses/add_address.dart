import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class AddAddress extends UseCase<bool, AddAddressParams> {
  final EcommerceRepository repository;
  AddAddress(this.repository);

  @override
  Future<Either<Failure, bool>> call(AddAddressParams params) async {
    return await repository.addAddress(
        cityId: params.cityId,
        countryId: params.countryId,
        name: params.name,
        stateId: params.stateId,
        description: params.description);
  }
}

class AddAddressParams {
  final int countryId;
  final int cityId;
  final int? stateId;
  final String name;
  final String description;

  AddAddressParams({
    required this.cityId,
    required this.countryId,
    required this.name,
    this.stateId,
    required this.description,
  });
}
