import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetProductDetails extends UseCase<Product, GetProductDetailsParams> {
  final EcommerceRepository repository;
  GetProductDetails(this.repository);

  @override
  Future<Either<Failure, Product>> call(GetProductDetailsParams params) async {
    return await repository.getProductDetails(id: params.id);
  }
}

class GetProductDetailsParams {
  final String id;
  GetProductDetailsParams({required this.id});
}
