import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetSearchHistory extends UseCase<List<String>?, NoParams> {
  final EcommerceRepository repository;
  GetSearchHistory(this.repository);

  @override
  Future<Either<Failure, List<String>?>> call(NoParams noParams) async {
    final result = repository.getLocalQueries();
    return Right(result);
  }
}
