import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetCommonSearch extends UseCase<List<Product>, NoParams> {
  final EcommerceRepository repository;
  GetCommonSearch(this.repository);

  @override
  Future<Either<Failure, List<Product>>> call(NoParams noParams) async {
    return await repository.getListProducts(endpoint: "common_searches");
  }
}
