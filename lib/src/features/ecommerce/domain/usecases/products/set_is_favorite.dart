import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class SetIsFavorite extends UseCase<bool, SetIsFavoriteParams> {
  final EcommerceRepository repository;
  SetIsFavorite(this.repository);

  @override
  Future<Either<Failure, bool>> call(SetIsFavoriteParams params) async {
    return await repository.setIsFavorites(
        isFavorite: params.isFavorite, itemId: params.itemId);
  }
}

class SetIsFavoriteParams {
  final bool isFavorite;
  final String itemId;
  SetIsFavoriteParams({required this.isFavorite, required this.itemId});
}
