// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CartModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartModel _$CartModelFromJson(Map<String, dynamic> json) => CartModel(
      id: json['id'] as int,
      uuid: json['uuid'] as String,
      totalText: json['total_text'] as String?,
      discountText: json['discount_text'] as String?,
      discountPriceText: json['discount_price_text'] as String?,
      cartItemsCount: json['cart_items_count'] as int?,
      subtotalText: json['subtotal_text'] as String?,
      addressesList: (json['addresses_list'] as List<dynamic>)
          .map((e) => Address.fromJson(e))
          .toList(),
      itemsList: (json['items_list'] as List<dynamic>)
          .map((e) => CartItem.fromJson(e))
          .toList(),
      feesText: json['fees_text'] as int?,
    );

Map<String, dynamic> _$CartModelToJson(CartModel instance) => <String, dynamic>{
      'id': instance.id,
      'uuid': instance.uuid,
      'subtotal_text': instance.subtotalText,
      'cart_items_count': instance.cartItemsCount,
      'discount_price_text': instance.discountPriceText,
      'total_text': instance.totalText,
      'discount_text': instance.discountText,
      'items_list': instance.itemsList,
      'addresses_list': instance.addressesList,
      'fees_text': instance.feesText,
    };
