import 'package:json_annotation/json_annotation.dart';

part 'my_points_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class MyPointsResponse {
  final int? currentPoints;
  final int? totalPoints;
  final int? usedPoints;
  final String? totalSaving;
  final String? message;

  MyPointsResponse({
    this.currentPoints,
    this.totalPoints,
    this.totalSaving,
    this.usedPoints,
    this.message
  });

  factory MyPointsResponse.fromJson(json) => _$MyPointsResponseFromJson(json);
  toJson() => _$MyPointsResponseToJson(this);

  static List<MyPointsResponse> fromJsonList(List json) {
    return json.map((e) => MyPointsResponse.fromJson(e)).toList();
  }
}
