import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/Address.dart';

import 'CartItem.dart';

part 'CartModel.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CartModel {
  final int id;
  final String uuid;
  final String? subtotalText;
  final int? cartItemsCount;
  final String? discountPriceText;
  final String? totalText;
  final String? discountText;
  final List<CartItem> itemsList;
  final List<Address> addressesList;
  final int? feesText;
  CartModel(
      {required this.id,
      required this.uuid,
      this.totalText,
      this.discountText,
      this.discountPriceText,
      this.cartItemsCount,
      this.subtotalText,
      required this.addressesList,
      required this.itemsList,
      this.feesText});

  factory CartModel.fromJson(json) => _$CartModelFromJson(json);
  toJson() => _$CartModelToJson(this);

  static List<CartModel> fromJsonList(List json) {
    return json.map((e) => CartModel.fromJson(e)).toList();
  }
}
