// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CheckoutModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckoutModel _$CheckoutModelFromJson(Map<String, dynamic> json) =>
    CheckoutModel(
      addressId: json['address_id'] as int,
      paymentMethod:
          $enumDecode(_$PaymentMethodsEnumMap, json['payment_method']),
      notes: json['notes'] as String? ?? "",
    );

Map<String, dynamic> _$CheckoutModelToJson(CheckoutModel instance) =>
    <String, dynamic>{
      'notes': instance.notes,
      'address_id': instance.addressId,
      'payment_method': _$PaymentMethodsEnumMap[instance.paymentMethod],
    };

const _$PaymentMethodsEnumMap = {
  PaymentMethods.cod: 'cod',
  PaymentMethods.wire_transfer: 'wire_transfer',
  PaymentMethods.skipcash: 'skipcash',
};
