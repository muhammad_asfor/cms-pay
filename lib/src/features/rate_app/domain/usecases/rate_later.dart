import 'package:dartz/dartz.dart';
import '../../../../../core.dart';
import '../repositories/rateApp_repository.dart';

class Ratelater extends UseCase<bool, NoParams> {
  final RateAppRepository repository;
  Ratelater(this.repository);

  @override
  Future<Either<Failure, bool>> call(NoParams noParams) async {
    return await repository.rateLater();
  }
}
