// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';

// class NotificationClass {
//   NotificationClass._();
//   static Function clickCallback;
//   FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;
//   static final NotificationClass notificationClass = NotificationClass._();
//   init() {
//     _getnotification;
//   }

//   Future<FlutterLocalNotificationsPlugin> get _getnotification async {
//     if (_flutterLocalNotificationsPlugin != null)
//       return _flutterLocalNotificationsPlugin;
//     else {
//       // final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
//       //     FlutterLocalNotificationsPlugin();
//       // final NotificationAppLaunchDetails notificationAppLaunchDetails =
//       //     await _flutterLocalNotificationsPlugin
//       //         .getNotificationAppLaunchDetails();

//       const AndroidInitializationSettings initializationSettingsAndroid =
//           AndroidInitializationSettings('@drawable/ic_stat_name');

//       /// Note: permissions aren't requested here just to demonstrate that can be
//       /// done later
//       final IOSInitializationSettings initializationSettingsIOS =
//           IOSInitializationSettings(
//               requestAlertPermission: true,
//               requestBadgePermission: true,
//               requestSoundPermission: true,
//               onDidReceiveLocalNotification:
//                   (int id, String title, String body, String payload) async {});

//       final InitializationSettings initializationSettings =
//           InitializationSettings(
//         android: initializationSettingsAndroid,
//         iOS: initializationSettingsIOS,
//       );
//       _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
//       await _flutterLocalNotificationsPlugin.initialize(
//         initializationSettings,
//         onSelectNotification: (String payload) async {
//           if (payload != null) {
//             //on luanching app
//             if (payload is String) {
//               clickCallback(payload);
//             }
//           }
//         },
//       );
//       return _flutterLocalNotificationsPlugin;
//     }
//   }

//   // var platform = MethodChannel('crossingthestreams.io/resourceResolver');

//   Future<void> showNotification(String title, String body, String payload,
//       {String sound}) async {
//     final noti = await NotificationClass.notificationClass._getnotification;
//     const AndroidNotificationDetails androidPlatformChannelSpecifics =
//         AndroidNotificationDetails(
//             'mussafer.com', 'mussafer', 'musafer channel',
//             importance: Importance.max,
//             priority: Priority.high,
//             ticker: 'ticker');
//     const NotificationDetails platformChannelSpecifics =
//         NotificationDetails(android: androidPlatformChannelSpecifics);
//     await noti.show(0, title, body, platformChannelSpecifics, payload: payload);
//   }

//   Future onDidReceiveLocalNotification(
//       int id, String title, String body, String payload) async {
//     // display a dialog with the notification details, tap ok to go to another page
//     // showDialog(
//     //   context: AppSnackBar.appContext,
//     //   builder: (BuildContext context) => CupertinoAlertDialog(
//     //     title: Text(title),
//     //     content: Text(body),
//     //     actions: [
//     //       CupertinoDialogAction(
//     //         isDefaultAction: true,
//     //         child: Text('new message'),
//     //         onPressed: () async {
//     //           Navigator.of(context, rootNavigator: true).pop();
//     //           if (payload != null) {
//     //             //on luanching app
//     //             if (payload is String) {
//     //               clickCallback(payload);
//     //             }
//     //           }
//     //         },
//     //       )
//     //     ],
//     //   ),
//     // );
//   }
// }
