
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/usecases/usecases.dart';
import 'package:uni_links/uni_links.dart';

class UniLinks {
  static bool didInitiLinks = false;

  static initDynamicLinks(Function(String id) callback) async {
    print('aaaaaa');
    final initialLink = await getInitialLink();
    print('bbbbbb');
    if (initialLink != null) {
      print('cccccc $initialLink');
      didInitiLinks = true;
      print("recieved url " + initialLink);
      final result = await Url2Id(sl()).call(Url2IdParams(url: initialLink));
      result.fold((l) {}, (r) {
        callback(r);
      });
    }
  }
}
