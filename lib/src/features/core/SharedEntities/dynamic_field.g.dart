// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dynamic_field.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DynamicField _$DynamicFieldFromJson(Map<String, dynamic> json) => DynamicField(
      id: json['id'] as int,
      title: json['title'] as String,
      type: json['type'] as String,
      options:
          (json['options'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$DynamicFieldToJson(DynamicField instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'type': instance.type,
      'options': instance.options,
    };
