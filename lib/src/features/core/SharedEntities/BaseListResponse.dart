import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/Link.dart';
part 'BaseListResponse.g.dart';



@JsonSerializable(
    fieldRename: FieldRename.snake, genericArgumentFactories: true)
class BaseListResponse<T> {
  final int? currentPage;
  final List<T> data;
  final String? firstPageUrl;
  final int? from;
  final int? lastPage;
  final String? lastPageUrl;
  final List<Link>? links;
  final String? nextPageUrl;
  final String? path;
  final int? perPage;
  final String? prevPageUrl;
  final int? to;
  final int? total;

  BaseListResponse({
    this.currentPage,
    required this.data,
    this.links,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  factory BaseListResponse.fromJson(
    Map<String, dynamic> json,
    fromJsonT,
  ) =>
      _$BaseListResponseFromJson(json, fromJsonT);
}
