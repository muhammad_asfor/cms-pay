import 'package:json_annotation/json_annotation.dart';
part 'dynamic_field.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class DynamicField {
  final int id;
  final String title;
  final String type;
  final List<String> options;
  // Null parent;

  DynamicField({
    required this.id,
    required this.title,
    required this.type,
    required this.options,
  });

  factory DynamicField.fromJson(json) => _$DynamicFieldFromJson(json);
  toJson() => _$DynamicFieldToJson(this);
}
