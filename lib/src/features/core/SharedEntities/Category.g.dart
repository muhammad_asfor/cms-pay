// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Category _$CategoryFromJson(Map<String, dynamic> json) => Category(
      id: json['id'] as int,
      subCategories: (json['sub_categories'] as List<dynamic>?)
          ?.map((e) => Category.fromJson(e))
          .toList(),
      parentId: json['parent_id'] as int?,
      featured: json['featured'] as int?,
      coverImage: json['cover_image'] as String,
      title: json['title'] as String,
      description: json['description'] as String?,
      dynamicFilterFields: (json['dynamic_filter_fields'] as List<dynamic>?)
          ?.map((e) => DynamicField.fromJson(e))
          .toList(),
    );

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'id': instance.id,
      'parent_id': instance.parentId,
      'featured': instance.featured,
      'cover_image': instance.coverImage,
      'title': instance.title,
      'description': instance.description,
      'sub_categories': instance.subCategories,
      'dynamic_filter_fields': instance.dynamicFilterFields,
    };
