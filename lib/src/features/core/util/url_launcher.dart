import 'package:url_launcher/url_launcher.dart';

openWhatsapp(String phone) {
  launch("https://wa.me/$phone");
}

sendMessageToWhatsapp(String phone, String message) {
  launch("https://api.whatsapp.com/send?phone=$phone&text=$message");
}
