import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Token.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';

class GetPublicToken extends UseCase<Token, NoParams> {
  final AuthRepository repository;
  GetPublicToken(this.repository);
  
  @override
  Future<Either<Failure, Token>> call(NoParams params) async {
    return await repository.getPublicToken();
  }
}
