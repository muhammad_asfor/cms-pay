import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';

class SetFcmToken extends UseCase<void, SetFcmTokenParams> {
  final AuthRepository repository;
  SetFcmToken(this.repository);
  @override
  Future<Either<Failure, void>> call(SetFcmTokenParams params) async {
    return await repository.setFcmToken(
      token: params.token,
    );
  }
}

class SetFcmTokenParams {
  final String token;

  SetFcmTokenParams({
    required this.token,
  });
}
