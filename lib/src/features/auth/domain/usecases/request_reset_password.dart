import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:equatable/equatable.dart';
import 'package:dartz/dartz.dart';

class RequestResetPassword extends UseCase<bool, RequestResetPasswordParams> {
  final AuthRepository repository;
  RequestResetPassword(this.repository);
  @override
  Future<Either<Failure, bool>> call(RequestResetPasswordParams params) async {
    return await repository.requestResetPassword(
      email: params.email,
    );
  }
}

class RequestResetPasswordParams {
  final String email;

  RequestResetPasswordParams({
    required this.email,
  });
}
