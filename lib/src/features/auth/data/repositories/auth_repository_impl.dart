import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/src/features/auth/data/datasources/AuthSharedPref.dart';
import 'package:progiom_cms/src/features/auth/data/datasources/auth_api.dart';
import 'package:progiom_cms/src/features/auth/domain/entities/LogginChecker.dart';

import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Token.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';

import '../../../../../homeSettings.dart';

class AuthRepositoryImpl extends AuthRepository {
  final AuthApi authApi;
  final AuthSharedPrefs sharedPrefs;
  AuthRepositoryImpl(this.authApi, this.sharedPrefs);
  @override
  Future<Either<Failure, bool>> changePassword(
      {required String email,
      required String password,
      required String resetCode}) async {
    try {
      final token = await authApi.changePassword(
          email: email, code: resetCode, password: password);

      return Right(token);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, User>> login(
      {required String email, required String password, Dio? tokenDio}) async {
    try {
      var user = await authApi.login(
          email: email, password: password, tkDio: tokenDio);
      sharedPrefs.saveAuthUser(user..password = password);
      sharedPrefs.setIsLogin(false);
      sl<HomesettingsBloc>().settings?.user = (user..password = password);
      return Right(user);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, String>> refreashToken(
      {required String refreashToken}) async {
    try {
      var token = await AuthApi.refreashToken(refreashToken);
      sharedPrefs.saveUserToken(
          token.accessToken ?? "", token.refreshToken ?? "");

      return Right(token.accessToken ?? "");
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> requestResetPassword(
      {required String email}) async {
    try {
      final token = await authApi.requestChangePassword(email: email);

      return Right(token);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, User>> signup(
      {required Map<String, dynamic> user}) async {
    try {
      var newUser = await authApi.register(
        user: user,
      );

      newUser.password = user["password"];
      sharedPrefs.saveAuthUser(newUser);
      sharedPrefs.setIsLogin(false);
      sl<HomesettingsBloc>().settings?.user = newUser;
      return Right(newUser);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> validateResetCode(
      {required String code, required String email}) async {
    try {
      final token = await authApi.validateResetCode(email: email, code: code);

      return Right(token);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, LogginChecker>> checkIfLoggedin() async {
    final result = await sharedPrefs.getIsLogin();
    if (result) {
      final token = await sharedPrefs.getUserToken();
      // logged in
      if (token?.isNotEmpty ?? false) {
        AuthApi.addTokenToHeader(token!, false);
        return Right(LogginChecker(
            isFirstLaunch: false, isLoggedIn: true, token: token));
      } else // loggedIn but no token then there is error in the flow
      {
        return Right(LogginChecker(
          isFirstLaunch: false,
          isLoggedIn: false,
        ));
      }
    } else {
      // not logged in
      final token = await sharedPrefs.getPublicToken();
      // have launched the app before
      if (token?.isNotEmpty ?? false) {
        AuthApi.addTokenToHeader(token!, true);
        return Right(LogginChecker(
            isFirstLaunch: false, isLoggedIn: false, token: token));
      } else
        // first launch
        return Right(LogginChecker(
          isFirstLaunch: true,
          isLoggedIn: false,
        ));
    }
  }

  // get user token
  @override
  Future<Either<Failure, Token>> getPublicToken() async {
    try {
      final token = await authApi.getPublicToken();
      sharedPrefs.setPublicToken(token.accessToken ?? "");
      return Right(token);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, void>> logout() async {
    await authApi.logout();
    await sharedPrefs.logout();

    return Right("");
  }

  @override
  Future<Either<Failure, User>> getMyProfile() async {
    try {
      final result = await authApi.getMyProfile();
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, void>> updateProfile(Map<String, dynamic> data) async {
    try {
      await authApi.updateProfile(data);
      return Right("");
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, User>> socialLogin(
      {required String token, required String provider}) async {
    try {
      var user = await authApi.socialLogin(provider: provider, token: token);
      sharedPrefs.saveAuthUser(user);
      sharedPrefs.setIsLogin(true);
      sl<HomesettingsBloc>().settings?.user = (user);
      return Right(user);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, void>> setFcmToken({required String token}) async {
    try {
      await authApi.setFcmToken(token);

      return Right("");
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
