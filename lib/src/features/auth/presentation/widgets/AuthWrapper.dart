import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progiom_cms/src/features/auth/presentation/bloc/auth_bloc.dart';

class AuthWrapper extends StatelessWidget {
  final AuthBloc authBloc;
  final Function(bool isAfterLogout, {String? token}) onTokenReady;
  final Function(String error) onErrorInToken;
  final Widget child;
  AuthWrapper(
      {Key? key,
      required this.authBloc,
      required this.child,
      required this.onTokenReady,
      required this.onErrorInToken})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: authBloc,
      listenWhen: (prev, next) {
        if (next is PublicTokenReady || next is ErrorInGettingPublicToken) {
          return true;
        }
        return false;
      },
      listener: (context, state) {
        if (state is PublicTokenReady) {
          onTokenReady(state.isAfterLogout, token: state.token);
        } else if (state is ErrorInGettingPublicToken) {
          onErrorInToken(state.error);
        }
      },
      child: child,
    );
  }
}
