part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class LoadingLogin extends AuthState {}

class LoadingSignUp extends AuthState {}

class ErrorInLogin extends AuthState {
  final String error;
  ErrorInLogin(this.error);
}

class ErrorSignUp extends AuthState {
  final String error;
  ErrorSignUp(this.error);
}

class LoginSuccess extends AuthState {
  final User user;
  LoginSuccess(this.user);
}

class SignUpSuccess extends AuthState {
  final User user;
  SignUpSuccess(this.user);
}

class PublicTokenReady extends AuthState {
  final bool isAfterLogout;
  final String? token;
  PublicTokenReady(this.isAfterLogout, {this.token});
}

class ErrorInGettingPublicToken extends AuthState {
  final String error;
  ErrorInGettingPublicToken(this.error);
}

class ProfileReady extends AuthState {
  final User user;
  ProfileReady(this.user);
}

class LoadingProfile extends AuthState {}

class ErrorInProfile extends AuthState {
  final String error;
  ErrorInProfile(this.error);
}

class LoadingUpdateProfile extends AuthState {}

class ErrorInUpdateProfile extends AuthState {
  final String error;
  ErrorInUpdateProfile(this.error);
}

class ProfileUpdated extends AuthState {}
